const TelegramApi = require("node-telegram-bot-api")

const token = "5467464570:AAFbQuNkLQ2V9-oIqH8p8h9rIIZgGyFfI9k"

const bot = new TelegramApi(token, {polling: true})

const toDoList = []

let checkWriteList = false
let checkTimer = false
let checkSetDelay = false

let workTime = 0
let restTime = 0


const toDoOptions = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{text: "Прекратить добавление", callback_data: "/stopadd"}]
        ]
    })
}


const outTDList = (subChatId) => {
    bot.sendMessage(subChatId, "Список твоих задач")
    toDoList.forEach(element => {
        bot.sendMessage(subChatId, element.toDo)
    });
}

const delaySendMessage = (subChatId) => {
    bot.sendMessage(subChatId, "Время вышло.")
}


const start = () => {
    bot.setMyCommands([
        {command: "/start", description: "Привветственное сообщение"},
        {command: "/info", description: "Информация о боте"},
        {command: "/settodolist", description: "Составить список задач на день"},
        {command: "/settimer", description: "Установить длительность таймера"},
    ])
   
    bot.on("message", async msg => {
        const text = msg.text;
        const chatId = msg.chat.id;
        
        
        if (checkWriteList) {
            toDoList.push({"toDo": text, "userId": chatId, 'tomatoNum': 0,})
            await bot.sendMessage(chatId, "Задача добавлена.", toDoOptions)
            return
        }
        if(checkSetDelay){
            restTime = Number(text)
            await bot.sendMessage(chatId, "Время таймера успешно установлено!")
            checkSetDelay = false
            await bot.sendMessage(chatId, `Время работы: ${workTime} мин. Время отдыха: ${restTime} мин.`)
        }
        if(checkTimer && !(checkSetDelay)){
            workTime = Number(text)
            checkSetDelay = true 
            await bot.sendMessage(chatId, "Время работы установлено. Теперь введите время отдыха в минутах.")           
        }
        

        if (text === "/start") {
            await bot.sendMessage(chatId, "Добро пожаловать в POMODORO BOT!")
            return
        }

        if (text === "/info") {
            bot.sendMessage(chatId, `Бот создан для удобного использования техники тайм-менеджмента Pomodoro.
Правила использования техники:
 1️⃣ - Составьте список задач, которые нужно сделать на сегодняшний день. Такие задачи называются активными.
 2️⃣ - Расставьте задачи на день от наиболее до наименее приоритетной
 3️⃣ - Включите таймер на 25 минут. Начинайте работу!
 4️⃣ - Прошло 25 минут — сделайте перерыв в 5-10 минут. Спустя 4 «помидора» сделайте полноценный перерыв в 15-20 минут.
 5️⃣ - Повторяйте пункты 1-4, пока не закроете все задачи из списка на день.`)   
            return                             
        }

        if (text === "/settodolist") {
            await bot.sendMessage(chatId, "Установить список задач, старайтесь вводить задачи от наиболее приоритетной.")
            checkWriteList = true
            return  
        }
        
        if(text === "/settimer"){
            await bot.sendMessage(chatId, "Настройка таймера работы и перерыва.")
            await bot.sendMessage(chatId, "Введите время работы в минутах.")
            checkTimer = true
        }

    })
        
    

    bot.on("callback_query", async msg => {
        const data = msg.data;
        const chatId = msg.message.chat.id;
        if(data === "/stopadd"){
            outTDList(chatId)
            checkWriteList = false            
        }
    })

   
}

start()